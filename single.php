<?php
get_header();
?>
<div class="content">
    <div class="container single_content">
        <?php
        $place = get_field('miejsce');
        $date = get_field('data');
        ?>
        <?php if ($date || $place) : ?>
            <div class="single__thumb"><?php the_post_thumbnail('large'); ?></div>
            <div class="events_item_meta">
                <?php if ($date) : ?>
                    <?php if ($date['poczatek'] && $date['koniec']) : ?>
                        <?php
                        $start = explode('/', $date['poczatek']);
                        $end = explode('/', $date['koniec']);

                        if ($start[1] == $end[1]) :
                        ?>
                            <span class="events_date"><?= __('Kiedy: ', 'devmn') . $start[0] . ' - ' . $end[0] . '/' . $end[1] . '/' . $end[2] ?></span>
                        <?php
                        else :
                        ?>
                            <span class="events_date"><?= __('Kiedy: ', 'devmn') . $start[0] . '/' . $start[1] . ' - ' . $end[0] . '/' . $end[1] . '/' . $end[2] ?></span>
                        <?php endif; ?>
                    <?php elseif ($date['poczatek']) : ?>
                        <span class="events_date"><?= __('Kiedy: ', 'devmn') . $date['poczatek'] ?></span>
                    <?php endif; ?>

                <?php endif; ?>
                <?php if ($place) : ?>
                    <span class="events_place"><?= __('Gdzie: ', 'devmn') . $place ?></span>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <div class="single__content"><?php the_content(); ?></div>
    </div>
    <?php $gallery = get_field('gallery');
    if ($gallery) : ?>
        <div class="subpage_gallery_items">
            <?php foreach ($gallery as $item) : ?>
                <div class="subpage_gallery_item">
                    <a href="<?php echo $item['sizes']['foundation-large']; ?>" rel="lightbox">
                        <span class="subpage_gallery_item_overlay"><i class="fas fa-search-plus"></i></span>
                        <img 
                        src=""
                        class="add_src"
                        data-large='<?php echo $item['sizes']['foundation-medium']; ?>'
                        data-small='<?php echo $item['sizes']['foundation-small']; ?>' 
                        alt="<?= $item['title'] ?>" 
                        loading="lazy"
                         />
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
<?php
get_footer();
