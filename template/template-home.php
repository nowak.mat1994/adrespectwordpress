<?php
/*
  Template Name: Strona główna
 */
get_header();
?>
<div class="inner-content">
  <?php get_template_part('template/parts/home', 'header'); ?>
  <?php get_template_part('template/parts/home', 'offer'); ?>
  <?php get_template_part('template/parts/home', 'about'); ?>
  <?php get_template_part('template/parts/home', 'gallery'); ?>
  <?php get_template_part('template/parts/home', 'cta'); ?>
</div>
<?php
get_footer();
?>