<section class="home_header">
    <?php
    $header = get_field('header'); ?>
    <div class="home_header_items">
        <?php
        foreach ($header as $item) :
        ?>
            <div class="home_header_item">
                <div class="container">
                    <div class="row align-items-stretch">
                        <div class="home_header_item_content col-12 col-md-6">
                            <div class="home_header_item_content_wrap">
                                <div class="home_header_item_title">
                                    <?= $item['title']; ?>
                                </div>
                                <div class="home_header_item_subtitle">
                                    <?= $item['subtitle']; ?>
                                </div>
                                <?php if ($item['urls']) : ?>
                                    <div class="home_header_item_urls">
                                        <?php foreach ($item['urls'] as $url) : ?>
                                            <a class="button" href='<?= $url['item']['url']; ?>' title='<?= $url['item']['name']; ?>'><?= $url['item']['title']; ?></a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="home_header_item_img col-12 col-md-6">
                            <img src="<?= $item['img']['sizes']['foundation-medium']; ?>">
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>