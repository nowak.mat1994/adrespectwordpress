<section class="home_offer  animation-in" id='offer'>
    <div class="container bottom">
        <div class="row">
            <div class="home_offer_header col-12 col-md-7 offset-md-1">
                <div class="home_offer_header_supertitle"><?php the_field('offer_supertitle'); ?></div>
                <div class="home_offer_header_title"><?php the_field('offer_title'); ?></div>
                <div class="home_offer_header_subtitle"><?php the_field('offer_subtitle'); ?></div>
            </div>
        </div>
        <?php
        $items = get_field('offer_items');
        if ($items) :
        ?>
            <div class="home_offer_items row">
                <?php foreach ($items as $item) : ?>
                    <div class="home_offer_item col-12 col-md-4 align-items-stretch">
                        <div class="home_offer_item_wrap">
                            <img src='<?= $item['icon']['url']; ?>'>
                            <div class="home_offer_item_title"><?= $item['title']; ?></div>
                            <div class="home_offer_item_subtitle"><?= $item['subtitle']; ?></div>
                            <a href='<?= $item['url']['url']; ?>' title='<?= $item['url']['title']; ?>'><?= $item['url']['title']; ?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    </section>