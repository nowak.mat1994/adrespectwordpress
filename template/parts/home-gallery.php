<section class="home_gallery  animation-in" id='realization'>
    <div class="transparent">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-11 offset-md-1">
                    <div class="home_gallery_supertitle"><?php the_field('realization_supertitle'); ?></div>
                    <div class="home_gallery_title"><?php the_field('realization_title'); ?></div>
                </div>
            </div>
        </div>
        <?php $galeries = get_field('realization_gallery');
        if ($galeries) :
        ?>
            <div class="home_gallery_items gradient" id='home_gallery_items'>
                <?php foreach ($galeries as $item) : ?>
                    <div class="home_gallery_item">
                        <a href="<?php echo $item['sizes']['foundation-medium']; ?>" rel="lightbox">
                            <span class="subpage_gallery_item_overlay"><i class="fas fa-search-plus"></i></span> 
                            <img src="<?php echo $item['sizes']['foundation-large']; ?>" class="add_src" loading="lazy" style='width: 100% ' />
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="home_gallery_show">
                <button class="button"><span><?php _e('Rozwiń', 'devmn');  ?></span> <i class="fas fa-arrow-down"></i></button>
            </div>
        <?php endif; ?>
    </div>
</section>