<section class="home_cta">
    <div class="container">
        <div class='row justify-content-center animation-in'>
            <div class="home_cta_content col-12 col-md-10 bottom ">
                <div class="home_cta_wrap">
                    <div class="home_cta_title">
                        <?php the_field('cta_title'); ?>
                    </div>
                    <div class="home_cta_right">
                        <div class="home_cta_subtitle">
                            <?php the_field('cta_subtitle'); ?>
                        </div>
                        <?php $url = get_field('cta_url'); ?>
                        <a class='button' href='<?= $url['url'] ?>'><?= $url['title']; ?></a>
                    </div>
                </div>
            </div>
        </div>
</section>