<section class="home_about animation-in" id='about'>
    <div class="container">
        <div class="row align-items-stretch">
            <div class="col-12 col-md-6 home_about_img">
                <div class="left">
                    <?php $img = get_field('about_img'); ?>
                    <img src='<?= $img['sizes']['foundation-medium']; ?>' />
                </div>
            </div>
            <div class="home_about_content col-12 col-md-5 offset-md-1 right">
                <div class="right">
                    <div class="home_about_wrap">
                        <div class="home_about_supertitle"><?php the_field('about_supertitle'); ?></div>
                        <div class="home_about_title"><?php the_field('about_title'); ?></div>
                        <div class="home_about_subtitle"><?php the_field('about_subtitle'); ?></div>
                        <?php $url = get_field('about_url'); ?>
                        <a class='button' href="<?= $url['url']; ?>"><?= $url['title']; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>