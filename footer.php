<footer class="footer">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <div class="footer-top">
                    <div class="footer-logo"> <img src="<?= get_stylesheet_directory_uri() . '/dist/svg/logo-white.svg'; ?>" /></div>
                    <?php $url = get_field('footer_url', 'option'); ?>
                    <div class='footer-slogan'><?php the_field('footer_slogan', 'option'); ?> <a class="button" href='<?= $url['url']; ?>'><?= $url['title']; ?></a></div>
                    <hr>
                    <div class="footer-nav">
                        <?php devmn_footer_nav(); ?>
                    </div>
                    <div class="footer-contact">
                        <?php
                        $phone = get_field('footer_phone', 'option');
                        $mail = get_field('footer_mail', 'option');
                        ?>
                        <a href='tel:<?= $phone; ?>'><?= $phone; ?></a>
                        <a href="mailto:<?= $mail; ?>"><?= $mail; ?></a>
                    </div>
                </div>
                <div class="inner-footer">
                    <div class="inner-footer-col">Prawa zastrzeżone © 2022</div>
                    <div class="inner-footer-col">
                        made by <a href='https://adrespect.pl/' target='_blank'  rel="nofollow"><img src="<?= get_stylesheet_directory_uri() . '/dist/svg/adrespect.svg'; ?>" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php
wp_footer();
?>

<!-- ADD Massanger -->
<?php //require get_template_directory() . '/functions/masanger.php';
?>

</body>

</html>