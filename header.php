<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <div class="header__wrap">
            <div class="container">

                <div class='header__branding'>
                    <?php

                    if (get_custom_logo()) : ?>

                        <?php the_custom_logo(); ?>

                    <?php else : ?>

                        <a href='<?= home_url(); ?>'>
                            <h1 class="site-title section-title"><?php bloginfo('name'); ?></h1>
                        </a>
                    <?php endif; ?>
                </div>

                <div class="header_right hide-for-medium">
                    <div class="header__nav">
                        <?php devmn_top_nav(); ?>
                    </div>
                    <div class="header_search">
                        <div class="header_search_form">
                            <?= get_search_form(); ?>
                        </div>

                        <i class="fas fa-search" id='search-button'></i>
                    </div>
                </div>

                <div class="header__nav_mobile show-for-medium">

                    <div class='header__nav_mobile_button'><i class="fas fa-bars"></i></span></div>
                    <div class='header__nav_mobile__content hide'>
                        <div class='header__nav_mobile__content_wrap'>
                            <span class="header__nav_mobile__content_close"><i class="fas fa-times"></i></span>
                            <?php devmn_top_nav(); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </header>

    <?php
    if (!is_front_page() && !is_page_template('template/template-contact.php')) :
        $header_img = get_field('header_img');
        if (!$header_img) {
            $header_img = get_field('header_img', 'option');
        }
    ?>
        <div class="page_header lazybg" data-background='<?= $header_img["sizes"]['foundation-x-large']; ?>'>

        </div>
    <?php
    endif;
    ?>