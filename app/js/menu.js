document.addEventListener("DOMContentLoaded", function () {
    let menuBtn = document.querySelector('.header__nav_mobile_button'),
        mobileMenu = document.querySelector('.header__nav_mobile__content'),
        mobileMenuHasChild = mobileMenu.querySelectorAll('.menu-item-has-children'),
        times = document.querySelector('.header__nav_mobile__content_close');
    if (menuBtn) {

        menuBtn.addEventListener('click', function () {
            mobileMenu.classList.toggle('hide');
        });

        times.addEventListener('click', function () {
            mobileMenu.classList.add('hide');
        });

        mobileMenuHasChild.forEach(element => {
            element.querySelector('a').addEventListener('click', function (e) {
                e.preventDefault();
                let content = element.querySelector('.sub-menu');

                if (content.style.maxHeight) {
                    content.style.maxHeight = null;
                } else {
                    content.style.maxHeight = content.scrollHeight + "px";
                }
            })
        })
 
    }

    let search = document.querySelector('.header_search_form'),
        searchButton = document.querySelector('.header_search i');

        if(search){
            searchButton.addEventListener('click', function(){
                search.classList.toggle('show');
            })
        }

});

