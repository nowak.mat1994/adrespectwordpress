jQuery(document).ready(function ($) {
    $('.home_header_items').slick({
        dots: false,
        speed: 300,
        infinite: true,
        autoplay: true,
        fade: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 5000,
        centerMode: true,
        cssEase: 'ease-out',
        arrows: true,
    });
}); 