document.addEventListener("DOMContentLoaded", function () {
    let galleryShowMoreButton = document.querySelector('.home_gallery_show button'),
    galleryShowMoreButtonWrap = document.querySelector('.home_gallery_show'),
        galleryItems = document.querySelector('.home_gallery_items');

    galleryShowMoreButton.addEventListener('click', (e) => {
        
        if (galleryItems.style.maxHeight) {
            galleryItems.style.maxHeight = null;
            galleryItems.classList.remove('show');
            galleryShowMoreButtonWrap.classList.remove('bottom');
            galleryShowMoreButton.querySelector('span').textContent = 'Rozwiń';
        } else {
            galleryItems.style.maxHeight = galleryItems.scrollHeight + "px";
            galleryItems.classList.add('show');
            galleryShowMoreButtonWrap.classList.add('bottom');
            galleryShowMoreButton.querySelector('span').textContent = 'Zwiń';
        }



    })
})