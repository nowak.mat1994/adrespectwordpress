document.addEventListener("DOMContentLoaded", function () {
    let items = document.querySelectorAll('.animation-in');

    if (items) {
        let windowHeight = window.innerHeight;
        window.addEventListener('scroll', function () {
            items.forEach(e => {
                if (this.scrollY > (e.offsetTop - windowHeight/2) + 100) {
                    e.classList.add('show');
                }
            })
        })
    }
})