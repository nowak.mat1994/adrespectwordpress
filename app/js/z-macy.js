var masonry = new Macy({
    container: '.home_gallery_items',
    trueOrder: true,
    waitForImages: true,
    mobileFirst: true,
    columns: 1,
    margin: {
        y: 16,
        x: '1%',
    },
    breakAt: {
        640: {
            columns: 2,
            margin: {
                y: 21.5,
                x: '2%',
            }
        },
        1200: {
            columns: 3,
            margin: {
                y: 43,
                x: '3%',
            }
        },
    },
});

masonry.recalculate();